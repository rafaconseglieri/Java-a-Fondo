/**
 * 
 */
package libro.cap01;

/**
 * @author RC
 * Ejercicio Apartado 1.3.3, P�gina 6
 */

import java.util.Scanner;

public class NombreEdadAltura {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Introduzca Nombre, Edad y Altura ");
		
		String nom = scanner.next();
		int eda = scanner.nextInt();
		double alt = scanner.nextDouble();
		
		System.out.println(	"Nombre: " + nom
							+ ", Edad: " + eda
							+ ", Altura (en cm): " + alt);

	}

}
