/**
 * 
 */
package libro.cap01;

/**
 * @author RC
 * Ejercicio Apartado 1.3.1, P�gina 4
 */

import java.util.Scanner;

public class HolaMundoNombre {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Introduzca su nombre ");
		
		String nom = scanner.nextLine();
		
		System.out.println("Hola Mundo: "+ nom);

	}

}
